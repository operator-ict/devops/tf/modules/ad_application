# Unreleased
## Added
## Changed
## Fixed

# [0.3.8] - 2024-12-30
## Changed
- Use object_id instead of id, solves `"0ddb6e5c-8ae8-XXX" -> "/servicePrincipals/0ddb6e5c-8ae8-XXX"` and compatibility with newer azuread version

# [0.3.7] - 2024-07-19
## Added
- Add optional attributes group_membership_claims, optional_claims

# [0.3.5] - 2023-11-29
## Added
- Add access for owners to the Key vault

# [0.3.4] - 2023-11-15
## Added
- Add required_resource_access

# [0.3.3] - 2023-11-15
## Changed
- Handle empty roles

# [0.3.2] - 2023-11-09
## Changed
- Replace deprecated properties

# [0.3.1] - 2023-09-14
## Added
- Add sign_in_audience and web

# [0.3.0] - 2023-03-03
## Changed
- Setup role assigment as map(role, scope)

# [0.2.0] - 2023-03-02
## Changed
- BC(renaming previous kv): Add param optional kv_name

## Added
- role assigment

# [0.1.0] - 2023-03-02
## Added
- Add param scope_assign_role
