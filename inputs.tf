variable "name" {
  type = string
}

variable "owner_ids" {
  type = list(string)
}

variable "location" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "kv_name" {
  type    = string
  default = ""
}

variable "fallback_public_client" {
  type    = bool
  default = false
}

variable "secret_name" {
  type = string
}

variable "roles" {
  type = list(object({
    role  = string
    scope = string
  }))
  default = []
}

variable "sign_in_audience" {
  type    = string
  default = "AzureADMyOrg"
}

variable "web" {
  type = object({
    redirect_uris = optional(list(string), [])
  })
  default = {}
}

variable "public_client" {
  type = object({
    redirect_uris = optional(list(string), [])
  })
  default = {}
}

variable "required_resource_access" {
  type = map(object({
    resource_app_id = optional(string)
    resource_access = list(object({
      id   = string
      type = string
    }))
  }))
  default = null
}

variable "group_membership_claims" {
  type = set(string)
  default = []
}

variable "optional_claims" {
  type = map(object({
    access_tokens = optional(list(object({
      name = string
      additional_properties = optional(set(string), [])
      essential = optional(bool, false)
    })), [])

    id_tokens = optional(list(object({
      name = string
      additional_properties = optional(set(string), [])
      essential = optional(bool, false)
    })), [])

    saml2_tokens = optional(list(object({
      name = string
      additional_properties = optional(set(string), [])
      essential = optional(bool, false)
    })), [])
  }))
  default = {}
}