data "azurerm_client_config" "current" {}

resource "azuread_application" "app" {
  display_name                   = var.name
  owners                         = var.owner_ids
  sign_in_audience               = var.sign_in_audience
  fallback_public_client_enabled = var.fallback_public_client

  dynamic "web" {
    for_each = var.web == null ? [] : ["X"]
    content {
      redirect_uris = var.web.redirect_uris
    }
  }

  dynamic "public_client" {
    for_each = var.public_client == null ? [] : ["X"]
    content {
      redirect_uris = var.public_client.redirect_uris
    }
  }

  dynamic "required_resource_access" {
    for_each = var.required_resource_access == null ? {} : var.required_resource_access
    content {
      resource_app_id = required_resource_access.value.resource_app_id

      dynamic "resource_access" {
        for_each = toset(required_resource_access.value.resource_access)
        content {
          id   = resource_access.value.id
          type = resource_access.value.type
        }
      }
    }
  }
  group_membership_claims = var.group_membership_claims

  dynamic "optional_claims" {
    for_each = var.optional_claims == null ? {} : var.optional_claims
    content {
      dynamic "access_token" {
        for_each = toset(optional_claims.value.access_tokens)
        content {
          name                  = access_token.value.name
          additional_properties = access_token.value.additional_properties
          essential             = access_token.value.essential
        }
      }

      dynamic "id_token" {
        for_each = toset(optional_claims.value.id_tokens)
        content {
          name                  = id_token.value.name
          additional_properties = id_token.value.additional_properties
          essential             = id_token.value.essential
        }
      }

      dynamic "saml2_token" {
        for_each = toset(optional_claims.value.saml2_tokens)
        content {
          name                  = saml2_token.value.name
          additional_properties = saml2_token.value.additional_properties
          essential             = saml2_token.value.essential
        }
      }
    }
  }


}

resource "azuread_service_principal" "sp" {
  client_id                    = azuread_application.app.client_id
  app_role_assignment_required = false
  owners                       = var.owner_ids
}

resource "azuread_application_password" "pass" {
  application_id    = azuread_application.app.id
  end_date_relative = "26280h"
}

resource "azurerm_key_vault" "kv" {
  name                            = var.kv_name != "" ? var.kv_name : "app-ad-${var.name}"
  location                        = var.location
  resource_group_name             = var.resource_group_name
  sku_name                        = "standard"
  tenant_id                       = data.azurerm_client_config.current.tenant_id
  enabled_for_template_deployment = true
  enable_rbac_authorization       = true
  soft_delete_retention_days      = 30
  purge_protection_enabled        = false
}

#data "azurerm_key_vault_secrets" "secrets" {
#  key_vault_id = azurerm_key_vault.kv.id
#}

resource "azurerm_key_vault_secret" "expected_secrets" {
  key_vault_id = azurerm_key_vault.kv.id
  name         = var.secret_name
  value        = azuread_application_password.pass.value
}

resource "azurerm_role_assignment" "ra" {
  for_each = {
    for val in coalesce(var.roles, []) : "${val.role}>${val.scope}" => val
  }
  principal_id         = azuread_service_principal.sp.object_id
  role_definition_name = each.value.role
  scope                = each.value.scope
}

resource "azurerm_role_assignment" "ra-kv" {
  for_each = toset(var.owner_ids)
  principal_id         = each.value
  role_definition_name = "Key Vault Secrets User"
  scope                = azurerm_key_vault.kv.id
}
